﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

    //config
    [SerializeField] float moveSpeed = 1f;

    //states

    //cached refs
    Rigidbody2D myRigidBody;

    //messages and methods

	void Start ()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
    }
	
	
	void Update ()
    {

        if (isFacingRight())
        {
            myRigidBody.velocity = new Vector2(moveSpeed, myRigidBody.velocity.y);
        }
        else
        {
            myRigidBody.velocity = new Vector2(-moveSpeed, myRigidBody.velocity.y);
        }
        
	}

    bool isFacingRight()
    {
        return transform.localScale.x > 0;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        transform.localScale = new Vector2(-(Mathf.Sign(myRigidBody.velocity.x)), transform.localScale.y);
    }

}
