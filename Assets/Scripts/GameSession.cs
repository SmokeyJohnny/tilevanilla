﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameSession : MonoBehaviour {

    [SerializeField] int playerLife = 3;
    [SerializeField] int score = 0;
    [SerializeField] Text LivesText;
    [SerializeField] Text ScoreText;

    private void Awake()
    {
        int numGameSession = FindObjectsOfType<GameSession>().Length;
        if (numGameSession > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        LivesText.text = playerLife.ToString();
        ScoreText.text = score.ToString();
    }

    public void TakeLife ()
    {
        if (playerLife > 1)
        {
            ProcessPlayerDeath();
        }
        else
        {
            ResetGameSession();
        }
    }


    public void AddScore(int scoreToAdd)
    {
        score = score + scoreToAdd;
        ScoreText.text = score.ToString();
    }

    private void ProcessPlayerDeath()
    {
        playerLife -- ;
        var currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
        LivesText.text = playerLife.ToString();

    }

    private void ResetGameSession()
    {
        playerLife = 3;
        score = 0;
        LivesText.text = playerLife.ToString();
        ScoreText.text = score.ToString();
        SceneManager.LoadScene("MainMenu");
    }
}
