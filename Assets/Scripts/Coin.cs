﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

    [SerializeField] AudioClip coinPickupSFX;
    [SerializeField] int coinScoreWorth = 100;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        AudioSource.PlayClipAtPoint(coinPickupSFX, Camera.main.transform.position);

        var gameSession = FindObjectOfType<GameSession>();
        gameSession.AddScore(coinScoreWorth);

        Destroy(gameObject);
    }
}
