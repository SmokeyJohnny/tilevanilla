﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {

    //config
    [SerializeField] float runSpeed = 5f;
    [SerializeField] float jumpSpeed = 5f;
    [SerializeField] float climbSpeed = 3f;
    [SerializeField] int playerDamage = 1;
    [SerializeField] float hitRate = 0.1f;

    public int playerHealth = 4;
    private float nextHit = 0f;

    //cached Refs
    Rigidbody2D myRigidBody;
    Animator myAnimator;
    CapsuleCollider2D myBodyCollider;
    BoxCollider2D myFeetCollider;
    SpriteRenderer spriteRenderer;

	//messages and methods
	void Start ()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        myBodyCollider = GetComponent<CapsuleCollider2D>();
        myFeetCollider = GetComponent<BoxCollider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

	void Update ()
    {
        if (playerHealth <= 0)
        {
            Dying();
        }
        Running();
        FlipSprite();
        Jumping();
        Climbing();
    }



    private void Running()  
    {
        float HorizontalMovement = CrossPlatformInputManager.GetAxis("Horizontal");
        Vector2 playerVelocity = new Vector2(HorizontalMovement * runSpeed, myRigidBody.velocity.y);
        myRigidBody.velocity = playerVelocity;
        if (HorizontalMovement != 0)
        { myAnimator.SetBool("Running", true);}
        else myAnimator.SetBool("Running", false);
    }

    private void FlipSprite()
    {
        bool playerHasHorizontalSpeed = Mathf.Abs(myRigidBody.velocity.x) > Mathf.Epsilon;
        if (playerHasHorizontalSpeed)
        {

            Debug.Log(myRigidBody.velocity.x);

            if ( myRigidBody.velocity.x > 0) // speed is positive, so facing right
            {
                Debug.Log("moving Right, facing right");
                transform.localScale = new Vector2(Mathf.Abs(transform.localScale.x), transform.localScale.y);
            }
            else // speed is not positive, so facing left
            {
                Debug.Log("moving left, facing left");
                if (transform.localScale.x > 0) // only flip to negative if not already negative
                {
                    transform.localScale = new Vector2((transform.localScale.x) * -1, transform.localScale.y);
                }
            }

            
            //transform.localScale = new Vector2(Mathf.Sign(myRigidBody.velocity.x), transform.localScale.y);
        }
    }

    private void Jumping()
    {
        if (!myFeetCollider.IsTouchingLayers(LayerMask.GetMask("Ground")) && !myFeetCollider.IsTouchingLayers(LayerMask.GetMask("Enemy"))) { return; }

        if (CrossPlatformInputManager.GetButtonDown("Jump"))
        {
            Vector2 jumpVelocityToAdd = new Vector2(myRigidBody.velocity.x, jumpSpeed);
            myRigidBody.velocity += jumpVelocityToAdd;
        }
    }

    private void Climbing()
    {
        if (myFeetCollider.IsTouchingLayers(LayerMask.GetMask("Ladder")))
        {
            myRigidBody.gravityScale = 0f;
            float VerticalMovement = CrossPlatformInputManager.GetAxis("Vertical");
            Vector2 playerVelocity = new Vector2(myRigidBody.velocity.x, VerticalMovement * climbSpeed);
            myRigidBody.velocity = playerVelocity;

            if (VerticalMovement != 0)
            {
                myAnimator.SetBool("Climbing", true);
            }
            else myAnimator.SetBool("Climbing", false);
        }
        else
        {
            myAnimator.SetBool("Climbing", false);
            myRigidBody.gravityScale = 1f;
        }
    }



    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Feet touching enemy layer");
        if (myFeetCollider.IsTouchingLayers(LayerMask.GetMask("Enemy")))
        {
            Debug.Log(collision.collider.isTrigger);

                GameObject enemyWhoIHit = collision.gameObject;
                EnemyHit enemyWhoIHitScript = enemyWhoIHit.GetComponent<EnemyHit>();

            
                enemyWhoIHitScript.EnemyDamaged(playerDamage);
        }
    }


    /*
     *(myFeetCollider.IsTouchingLayers(LayerMask.GetMask("Enemy")))
        {
            if (Time.time > nextHit)
            {
                nextHit = Time.time + hitRate;
                GameObject enemyWhoIHit = collision.gameObject;
                EnemyHit enemyWhoIHitScript = enemyWhoIHit.GetComponent<EnemyHit>();
                enemyWhoIHitScript.EnemyDamaged(playerDamage);
            }
        }
        else if  
     */


    private void OnCollisionStay2D(Collision2D collision)
    {
        if (myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Enemy")))
        {
            Debug.Log("body touching enemy layer");
            GameObject enemyWhoHitMe = collision.gameObject;
            EnemyHit enemyWhoHitMeScript = enemyWhoHitMe.GetComponent<EnemyHit>();
            enemyWhoHitMeScript.DamagePlayer();
        }
        else if (myFeetCollider.IsTouchingLayers(LayerMask.GetMask("Hazards")) || myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Hazards")))
        {
            Debug.Log("feet touching hazard / enemy layer");
            playerHealth--;
        }
        else
        {
            return;
        }
    }


    
    private void Dying()
    {
        myAnimator.SetBool("isDead", true);

        myBodyCollider.enabled = false;
        myFeetCollider.enabled = false;

        spriteRenderer.sortingLayerName = "DeadLayer";

        float VerticalMovement = 10;

        Vector2 playerVelocity = new Vector2(myRigidBody.velocity.x, VerticalMovement * climbSpeed);
        myRigidBody.velocity = playerVelocity;

        runSpeed = 0f;
        jumpSpeed = 0f;
        climbSpeed = 0;

        Invoke("stopPlayerFromFalling",0.8f);

        var gameSession = FindObjectOfType<GameSession>();
        gameSession.TakeLife();
    }

    private void stopPlayerFromFalling()
    {
        myRigidBody.simulated = false;
    }


}
