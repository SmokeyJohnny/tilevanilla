﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHit : MonoBehaviour {

    [SerializeField] int enemyHealth = 3;
    [SerializeField] int enemyDamage = 1;

    CapsuleCollider2D myBodyCollider;
    BoxCollider2D myWallCollider;

    public void EnemyDamaged(int playerDamage)
    {
        Debug.Log("Enemy Health before hitting it: " + enemyHealth);
        if ( enemyHealth - playerDamage > 0)
        {
            enemyHealth = enemyHealth - playerDamage;
            Debug.Log("Enemy Health after hitting it: " + enemyHealth);
        }
        else
        {
            Debug.Log("Enemy health would be: so deleting the enemy object");
            Destroy(gameObject);
        }
    }

    public void DamagePlayer()
    {
        var player = FindObjectOfType<Player>();
        player.playerHealth = player.playerHealth - enemyDamage;
    }


}
